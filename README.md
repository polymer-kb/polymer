## Polymer Keyboard

Modular mechanical keyboard written in Rust.

![The Polymer Keyboard](polymer.jpg)

### Links

* [Overview blog post](https://josh.robsonchase.com/rest-of-the-keyboard)
* Main firmware repository: [firmware/polymer](https://gitlab.com/polymer-kb/firmware/polymer)
* Abstract keyboard core: [firmware/keebrs](https://gitalb.com/polymer-kb/firmware/keebrs)
* Embedded Futures executor: [firmware/embedded-executor](https://gitlab.com/polymer-kb/firmware/embedded-executor)
  * [Blog post!](https://josh.robsonchase.com/embedded-executor)
* stm32f103xx async abstractions: [firmware/stm32f103xx-futures](https://gitlab.com/polymer-kb/firmware/stm32f103xx-futures)
  * [Blog post coming eventually!](https://josh.robsonchase.com/async-serial)
* Case: [hardware/case](https://gitlab.com/polymer-kb/hardware/case)
  * Purchase case: [Armattan Store](https://armattanproductions.com/pages/shop_product_grid/4150)
* PCB: [hardware/pcb](https://gitlab.com/polymer-kb/hardware/pcb)
* Misc. Blog Posts
  * [Bootstrapping My Embedded Rust Development Environment](https://josh.robsonchase.com/embedded-bootstrapping/)
  * [Embedded Frustrations](https://josh.robsonchase.com/embedded-frustrations/)

### Key Features

* Rust firmware
  * written in an async-first style using the latest and greatest `Future`s API
  * Programmable layers
  * Complex(ish) actions: single key, key combinations, function calls, and
    sequences of actions (including other sequences!)
    * [Example layout](https://gitlab.com/polymer-kb/firmware/polymer/-/blob/master/src/layout.rs)
* ARM microcontroller
* Daisy-chainable
  * Mix-n-match modules

### MVP

The first iteration of the keyboard implementing the bare minimum features.

* Split ortholinear layout (5x7 x2)
  * ARM controller + 2 JST connectors per half
* RobotDyn STM32Mini controllers
  ([link](https://robotdyn.com/stm32f103-stm32-arm-mini-system-dev-board-stm-firmware.html))

Mix-n-match modules will probably come later.

Full BOM to come once I have it fully built and working.

### TODO

* Build guide
* Better support for function actions
* Lighting